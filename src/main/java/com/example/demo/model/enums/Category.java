package com.example.demo.model.enums;

public enum Category {
    LATEX , FOIL , ACCESSORIES, ELECTRONICS
}
